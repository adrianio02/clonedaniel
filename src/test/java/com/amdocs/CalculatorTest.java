package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.amdocs.Calculator;

public class CalculatorTest {
    @Test
    public void testAdd() throws Exception {

        int key= new Calculator().add();
        assertEquals("Add", 9, key);
        
    }
   
    @Test
    public void testSub() throws Exception {

        int key= new Calculator().sub();
        assertEquals("Sub", 3, key);

    }
}
