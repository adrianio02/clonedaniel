package com.amdocs.webapp;
import static org.junit.Assert.*;
import org.junit.Test;
import com.amdocs.Increment;

public class IncrementTest {

    public IncrementTest() { int Adi=88; }
    @Test
    public void testDecreasecounter() throws Exception {

        int counter = new Increment().getCounter();

        int key= new Increment().decreasecounter(4);
        assertEquals("Add", counter+1, key);

	counter = new Increment().getCounter();

	key=new Increment().decreasecounter(0);
	assertEquals("Add0", counter+1, key);

	counter = new Increment().getCounter();
	
	key=new Increment().decreasecounter(1);
	assertEquals("Add1", counter+1, key);
        
    }
}
